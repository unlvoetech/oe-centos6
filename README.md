# OE CentOS 6#


### What is this repository for? ###

* Vagrant/puppet setup for configuring a centos box on the unlv network using ace authentication
* Version 1.x

### How do I get set up? ###

* Download vagrant
* vagrant plugin install vagrant-vbguest
* Download repo
* Run vagrant up from repo directory

### Who do I talk to? ###

* UNLV Online Education