# Install ACE

class ace::install {

  $packages = [ "authconfig","krb5-workstation","pam_krb5","samba-common","oddjob-mkhomedir"
  
  package { 'download packages':
	$packages: ensure => "installed",
	notify => "run authconfig"
}

  exec { "run authconfig":
	refreshonly => true,
	command => "authconfig --disablecache --enablewinbind --enablewinbindauth --smbsecurity=ads --smbworkgroup=AD --smbrealm=AD.UNLV.EDU --enablewinbindusedefaultdomain --winbindtemplatehomedir=/home/ad.unlv.edu/%U --winbindtemplateshell=/bin/bash --enablekrb5 --krb5realm=AD.UNLV.EDU --enablekrb5kdcdns --enablekrb5realmdns --enablelocauthorize --enablemkhomedir --enablepamaccess --updateall"
	subscribe => Package['splunk'],
}
